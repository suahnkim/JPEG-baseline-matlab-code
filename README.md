# JPEG baseline matlab code
 Produces JFIF compliant baseline JPEG images (grayscaled & color & Huffman optimized version)

# Installation
- Download zip from github
- Install using git 
```
git clone https://github.com/suahnkim/JPEG-baseline-matlab-code
```

# Usage
- Run main.m from matlab terminal. 
```
main
```

# Dependencies
- Toolboxes from MATLAB

# Tested platform
- OS: Windows 10 
- MATLAB version: MATLAB R2021a

# Code repositories
The same code can be found @ gitlab: https://gitlab.com/suahnkim/JPEG-baseline-matlab-code and gitee: https://gitee.com/suahnkim/JPEG-baseline-matlab-code

# Contributor
This repository is maintained by Suah Kim @ https://iaslab.org.
<br>You can send email @ suahnkim(at)gmail.com for comments or requests.

# Donate
You can send me ethereum: 0x8ee66791696a81add6a2dab71e7833c4d2358a91
<br> Or buy me a cup of coffee: https://www.buymeacoffee.com/suahnkim 
